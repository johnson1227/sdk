
#ifndef __EasyOnvifClient_H__
#define __EasyOnvifClient_H__

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef _WIN32
#define Easy_API  __declspec(dllexport)
#define Easy_APICALL  __stdcall
#define WIN32_LEAN_AND_MEAN
#else
#define Easy_API
#define Easy_APICALL 
#endif

typedef enum EasyDarwinPTZActionType
{
	EASY_PTZ_ACTION_TYPE_CONTINUOUS = 1,				///< CONTINUOUS
	EASY_PTZ_ACTION_TYPE_SINGLE 						///< SINGLE
}EasyDarwinPTZActionType;

typedef enum
{
	EASY_PTZ_CMD_TYPE_STOP = 1,				    ///< STOP
	EASY_PTZ_CMD_TYPE_UP, 						///< UP
	EASY_PTZ_CMD_TYPE_DOWN, 						///< DOWN
	EASY_PTZ_CMD_TYPE_LEFT, 						///< LEFT
	EASY_PTZ_CMD_TYPE_RIGHT, 					///< RIGHT
	EASY_PTZ_CMD_TYPE_LEFTUP, 					///< LEFTUP
	EASY_PTZ_CMD_TYPE_LEFTDOWN, 					///< LEFTDOWN
	EASY_PTZ_CMD_TYPE_RIGHTUP, 					///< RIGHTUP
	EASY_PTZ_CMD_TYPE_RIGHTDOWN, 				///< RIGHTDOWN
	EASY_PTZ_CMD_TYPE_ZOOMIN, 					///< ZOOMIN
	EASY_PTZ_CMD_TYPE_ZOOMOUT, 					///< ZOOMOUT
	EASY_PTZ_CMD_TYPE_FOCUSIN, 					///< FOCUSIN
	EASY_PTZ_CMD_TYPE_FOCUSOUT, 					///< FOCUSOUT
	EASY_PTZ_CMD_TYPE_APERTUREIN, 				///< APERTUREIN
	EASY_PTZ_CMD_TYPE_APERTUREOUT, 				///< APERTUREOUT
}EasyPTZCMDType;

typedef enum 
{
	EASY_PRESET_CMD_TYPE_GOTO = 1,				    ///< GOTO
	EASY_PRESET_CMD_TYPE_SET, 						///< SET
	EASY_PRESET_CMD_TYPE_REMOVE, 					///< REMOVE

}EasyDarwinPresetCMDType;


typedef struct {
	char host[64];
	char deviceUrl[128];
}EasyOnvifDeviceInfo;

typedef struct {
	unsigned int deviceNum;
	EasyOnvifDeviceInfo* devices;
} EasyOnvifClient_DiscoverDevices;

typedef struct {
	char deviceUrl[128];
	char streamUrl[128];
} EasyOnvifClient_DeviceProbeInfo;

typedef struct {
	char presetName[128];
	char presetToken[128];
}EasyOnvifPreset;

typedef struct {
	unsigned int presetNum;
	EasyOnvifPreset* preset;
} EasyOnvifClient_Presets;

/*

*/
Easy_API int Easy_APICALL EasyOnvifClient_Init (const char* license);

/*

*/
Easy_API EasyOnvifClient_DiscoverDevices* Easy_APICALL EasyOnvifClient_GetDiscoveryDevices (void);

/*

*/
Easy_API EasyOnvifClient_DeviceProbeInfo* Easy_APICALL EasyOnvifClient_Probe (const char* host, const char* username, const char* password);

/*

*/
Easy_API int Easy_APICALL EasyOnvifClient_StartPtz (EasyPTZCMDType command, unsigned int speed, const char* host, const char* username, const char* password, const char* deviceUrl);

/*

*/
Easy_API int Easy_APICALL EasyOnvifClient_StopPtz (const char* host, const char* username, const char* password, const char* deviceUrl);

/*

*/
Easy_API int Easy_APICALL EasyOnvifClient_GotoPreset (const char* token, const char* host, const char* username, const char* password, const char* deviceUrl);

/*

*/
Easy_API int Easy_APICALL EasyOnvifClient_SetPreset (const char* name, const char* token, const char* host, const char* username, const char* password, const char* deviceUrl);

/*

*/
Easy_API int Easy_APICALL EasyOnvifClient_RemovePreset (const char* token, const char* host, const char* username, const char* password, const char* deviceUrl);

Easy_API EasyOnvifClient_Presets* Easy_APICALL EasyOnvifClient_GetPresetList(const char* host, const char* username, const char* password, const char* deviceUrl);

/*

*/
Easy_API int Easy_APICALL EasyOnvifClient_GetSnapshot (const char* host, const char* username, const char* password, void** ppdata, int maxsize);

/*

*/
Easy_API void Easy_APICALL EasyOnvifClient_Free(void* ptr);

/*

*/
Easy_API void Easy_APICALL EasyOnvifClient_Deinit (void);

//int EasyOnvifClientTest(void);


#ifdef __cplusplus
}
#endif
#endif

